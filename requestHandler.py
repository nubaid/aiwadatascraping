import requests, datetime, settings, sys, math
import myLogging as log
import repositoryMySQL as repo
import ImageDownloader as imgDownloader

#not used right now
def GetBrandByCompanyId(cIds):
    url = "https://aiwa.ae/api/services/app/company/GetCompanyBrands"
    bId = []
    stime = datetime.datetime.now()
    for c in cIds:
        payload = { "id" : c}
        try:
            response = GetResponse(url, payload)            
            if response is not 0:                
                for brand in response["items"]:                    
                    _brand = brand["brand"]
                    if _brand["id"] not in bId:
                        bId.append( _brand["id"])
                        imgName = _brand["nameEng"].replace(" ","")
                        imgurl = f"https://aiwa.ae/{_brand['logo']}"
                        imgDownloader.DownloadImage(settings.DOWNLOAD_BRAND_LOGO_PATH, imgurl, imgName)
                        log.createLog(f'Brand processed successfuly ID: {_brand["id"]} NAME: {_brand["nameEng"]}')
                        repo.InsertBrand(_brand)
                    else:
                        log.createLog(f'[INFO] Brand already added ID: {_brand["id"]} NAME: {_brand["nameEng"]}')

        except:
            e = sys.exc_info()
            log.createLog(f"ERROR {e}")

    etime = datetime.datetime.now()
    t = etime - stime
    log.createLog(f'Total Time in processing \"GetBrandAndCompanyByCompanyId\" {t}')
    pass

def GetCompanyDetailsByUniqueName(uniqueNames):
    companyIds = []
    url = "https://aiwa.ae/api/services/app/company/GetCompanyProfile"

    stime = datetime.datetime.now()
    for name in uniqueNames:
        payload = { "uniqueName" : name}
        try:
            response = GetResponse(url, payload)
            if response is not 0:
                companyIds.append(response["id"])                
                repo.InsertCompany(response)
        except:
            e = sys.exc_info()
            log.createLog(f"ERROR {e}")

    etime = datetime.datetime.now()
    t = etime - stime
    log.createLog(f'Total Time in processing \"GetCompanyDetailsByUniqueName\" {t}')
    return companyIds

def GetCompaniesByCategory(cIds):
    companyIds = []
    uniqueName = []

    url = "https://aiwa.ae/api/services/app/search/GetSearchResults"    

    stime = datetime.datetime.now()
    for ids in cIds:
        c = ids[0]
        payload_getcount = {
            "query": f"{c}",
            "searchField":"CategoryId",
            "sortByField":"relevance",
            "filters":"",
            "pageIndex":1,
            "pageSize":1,
            "params":"origin_region=undefined"
            
        }
        try:
            count_response = GetResponse(url, payload_getcount)
            if count_response is not 0:
                log.createLog(f'[INFO] Total company count: {count_response["result"]["totalCount"]} For category: {c}')
                pages = math.ceil(int(count_response["result"]["totalCount"])/int(settings.CHUNK_SIZE))
                chunksize = int(settings.CHUNK_SIZE)
                log.createLog(f"Total Chunks: {pages} Category: {c}")
                i = 1
                count = 1
                while count <= pages:
                    log.createLog(f"Processing Chunk # {count} For Category: {c}")
                    payload = {
                        "query": f"{c}",
                        "searchField":"CategoryId",
                        "sortByField":"relevance",
                        "filters":"",
                        "pageIndex":count,
                        "pageSize":chunksize,
                        "params":"origin_region=undefined"
                        
                    }

                    response = GetResponse(url, payload)
                    if response is not 0:
                        for company in response["result"]["items"]:
                            log.createLog(f'[INFO] Processing company # {i}')
                            if company["companyId"] not in companyIds:
                                companyIds.append(company["companyId"])
                                uniqueName.append(company["uniqueName"])
                                imgName = company["nameEng"].replace(" ","")
                                imgurl = f"https://aiwa.ae/{company['logo']}"
                                imgDownloader.DownloadImage(settings.DOWNLOAD_COMPANY_LOGO_PATH, imgurl, imgName)
                                log.createLog(f'Company processed successfuly ID: {company["companyId"]} NAME: {company["nameEng"]}')
                            else:
                                log.createLog(f'[WARNING]Company already added ID: {company["companyId"]} NAME: {company["nameEng"]}')
                            i += 1
                    count+=1
        except:
            e = sys.exc_info()
            log.createLog(f"ERROR {e}")

    etime = datetime.datetime.now()
    t = etime - stime
    log.createLog(f'Total Time in processing \"GetCompaniesByCategory\" {t}')
    return uniqueName

#not used
def GetCategoryIdsBySlug(slug):    
    Ids = []
    url = "https://aiwa.ae/api/services/app/category/GetCategoryBySlug"

    stime = datetime.datetime.now()

    for c in slug:
        payload = { "slug" : c}
        response = GetResponse(url, payload)
        if response is not 0:
            Ids.append(response["id"])
            repo.InsertCategory(response)            

    etime = datetime.datetime.now()
    t = etime - stime

    log.createLog(f'Total Time in processing \"GetCategoryBySlug\" {t}')
    return Ids

def GetResponse(url, payload):
    header = {"content-type": "application/json;charset=UTF-8"}    
    
    log.createLog(f'Request sent to {url} with payload {payload}')

    response = requests.post(url, headers=header, json=payload).json()
    #print(f'success: {response["success"]} \nerror: {response["error"]} \nResult :{response["result"]}')
    if response["success"] is False or response["result"] is None:
        log.createLog(f"ERROR processing request URL: {url} Payload: {payload}")
        return response["error"]["code"] if response["error"] is not None else 0
    else:
        log.createLog(f'Request successful')
        return response["result"]

def GetAllPossibleCategories(combination):
    cIds = []
    stime = datetime.datetime.now()
    for c in combination:

        log.createLog(f"Processing combination: {c}")
        url = "https://aiwa.ae/publicapi/GetAutoCompleteSuggestionsByType"
        header = {"content-type": "application/json;charset=UTF-8"}

        param = {'q': c, 'type' : 'category'}

        try:
            response = requests.post(url, headers=header, params=param).json()
            if len(response) is not 0:
                category = []
                for cat in response:
                    if cat['id'] not in cIds:
                        cIds.append(cat['id'])
                        category.append(tuple((cat['id'], cat['name'], cat['slug'])))
                    else:
                        log.createLog(f'[WARNING] Category already added ID: {cat["id"]} NAME: {cat["name"]}')  
                repo.InsertCategory(category)
                log.createLog(f'Categories Added Successfuly: {category}')
        except :
            e = sys.exc_info()
            log.createLog(f"ERROR {e}")      

    etime = datetime.datetime.now()
    t = etime - stime

    log.createLog(f'Total Time in processing \"GetAllPossibleCategories\" {t}')
    return cIds            



