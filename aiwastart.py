import requestHandler as request
import settings #my project settings
import myLogging as log
from pathlib import Path
import os, string, csv, sys, datetime
import repositoryMySQL as repo



def startmain():

    st = datetime.datetime.now()
    log.createLog(f'============================================ PROGRAM START [{st}] ============================================ ')
    
    try:
        ids = GetCategoryIds()    
        uniquename = request.GetCompaniesByCategory(ids)
        cIds = request.GetCompanyDetailsByUniqueName(uniquename)
        request.GetBrandByCompanyId(cIds)

    except:
        e = sys.exc_info()
        log.createLog(f"ERROR {e}")

    et = datetime.datetime.now()
    d = et - st
    log.createLog(f'TOTAL TIME : {d}\n============================================ PROGRAM ENDED [{et}] ============================================ ')


def GetCategoryIds():
    log.createLog(f'Scrap categories from AIWA: {settings.SCRAPE_CATEGORY}')
    if settings.SCRAPE_CATEGORY is True:
        stime = datetime.datetime.now()
        log.createLog(f'Scrap of categories started')
        letter = list(string.ascii_lowercase)
        combinations = []

        for i in range(0,26):
            for j in range(0,26):
                for k in range(0,26):
                    combinations.append(f"{letter[i]}{letter[j]}{letter[k]}")
        
        ids = request.GetAllPossibleCategories(combinations)

        etime = datetime.datetime.now()
        d = etime - stime
        log.createLog(f'Total Categories scrapped: {len(ids)}')
        log.createLog(f'Total Time in processing \"GetCategoryIds\" {d}')
        return ids
    else:
        ids = repo.GetCategoryIds()
        log.createLog(f'Total Categories retrived from DataBase: {len(ids)}')
        return ids


#not used
def test():
    ids = GetCategoryIds()
    # letter = list(string.ascii_lowercase)
    # combinations = []
    # for i in range(0,26):
    #     for j in range(0,26):
    #         for k in range(0,26):
    #             combinations.append(f"{letter[i]}{letter[j]}{letter[k]}")

    # ids = request.GetAllPossibleCategories(combinations)
    # print(ids)

#not used
def CheckFileExists(path):
    if not os.path.exists(path):    
        letter =list(string.ascii_lowercase)
        file = open(f"{settings.COMBINATION_CSV_NAME}","w+")

        for i in range(0,26):
            for j in range(0,26):
                for k in range(0,26):
                    file.write(f"{letter[i]}{letter[j]}{letter[k]}\n")

        file.close()
        return False
    else:
        return True
        
#not used
def ReadCSV(path):
    categories = []
    #filepath = settings.CSVPATH   
    with open(path) as csvfile:
        readCsv = csv.reader(csvfile)
        for row in readCsv:
            categories.append(row[0])
    return categories


if __name__ == "__main__":
    startmain()
    