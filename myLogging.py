import settings
import datetime
import os
from pathlib import Path


def createLog(msg):

    path = settings.ERRORLOGPATH
    if not os.path.exists(path):
        Path(path).mkdir(parents=True)

    filename = datetime.date.today()
    path = f"{path}\\{filename}.txt"    
    f = open(path, "a+")
    f.write(f"[{datetime.datetime.now()}] {msg}\n")
    f.close()
    print(f"[{datetime.datetime.now()}] {msg}\n")