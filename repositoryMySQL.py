import mysql.connector
import settings

mydb = mysql.connector.connect(
    host= settings.HOST_NAME,
    user= settings.USERNAME,
    passwd= settings.PASSWORD
)



def CheckDataBases():
    conn = mydb.cursor()
    statement = "SHOW DATABASES"
    conn.execute(statement)

    for db in conn:
        print(db)

def InsertCategory(categories):
    
    conn = mydb.cursor()
    statement = "INSERT IGNORE INTO aiwa.categories (SourceId, Name, Slug) VALUES (%s, %s, %s);"
    #category = (int(result_json["id"]), result_json["nameEng"], result_json["slug"], result_json["metaTitleEng"])
    
    conn.executemany(statement, categories)
    mydb.commit()


def InsertCompany(result_json):
    conn = mydb.cursor()
    statement = """REPLACE INTO aiwa.company  ( SourceId, name, instagramUrl, themeColor, brochureLink, primaryGpsLocation, hasVideos, shortDescription, primaryPhone, logo, hasCoupons, 
                    metaDescription, isFeatured, googlePlusUrl, linkedInUrl, iso, primaryFax, hasOffers, isVerified, description, tradeLicenceNumber, twitterUrl, poBox, primaryMobile, totalReviews, windowsStoreUrl, 
                    uniqueName, address, appleStoreUrl, primaryWebsite, userName, overallRating, primaryEmail, domainName, metaTitle, isPublished, googlePlaystoreUrl, facebookUrl, establishmentDate, isGreen, blackBerryStoreUrl )  
                    VALUES ( %s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s);"""

    company = (
    int(result_json["id"]), 
    result_json["nameEng"], 
    result_json["instagramUrl"], 
    result_json["themeColor"], 
    result_json["brochureLink"], 
    result_json["primaryGpsLocation"], 
    result_json["hasVideos"], 
    result_json["shortDescriptionEng"], 
    result_json["primaryPhone"], 
    result_json["logo"], 
    result_json["hasCoupons"], 
    result_json["metaDescription"],
    result_json["isFeatured"], 
    result_json["googlePlusUrl"], 
    result_json["linkedInUrl"], 
    result_json["iso"], 
    result_json["primaryFax"], 
    result_json["hasOffers"], 
    result_json["isVerified"], 
    result_json["descriptionEng"],
    result_json["tradeLicenceNumber"], 
    result_json["twitterUrl"], 
    result_json["poBox"], 
    result_json["primaryMobile"], 
    result_json["totalReviews"], 
    result_json["windowsStoreUrl"], 
    result_json["uniqueName"], 
    result_json["address"], 
    result_json["appleStoreUrl"], 
    result_json["primaryWebsite"], 
    result_json["userName"], 
    result_json["overallRating"], 
    result_json["primaryEmail"], 
    result_json["domainName"], 
    result_json["metaTitle"], 
    result_json["isPublished"], 
    result_json["googlePlaystoreUrl"], 
    result_json["facebookUrl"], 
    result_json["establishmentDate"], 
    result_json["isGreen"], 
    result_json["blackBerryStoreUrl"])

    conn.execute(statement, company)
    mydb.commit()


def InsertBrand(result_json):
    conn = mydb.cursor()
    statement = "REPLACE INTO aiwa.brand (SourceId, Name, LogoUrl) VALUES (%s, %s, %s);"
    brand = (int(result_json["id"]), result_json["nameEng"], result_json["logo"])
    
    conn.execute(statement, brand)
    mydb.commit()

def GetCategoryIds():

    conn = mydb.cursor()
    statement = "SELECT SourceId FROM aiwa.categories;"
    conn.execute(statement)
    records = conn.fetchall()
    return records