import requests
import shutil
from pathlib import Path
import myLogging as log
import os
import settings

def DownloadImage(path, url, imgname):

    #path = settings.DOWNLOAD_COMPANY_LOGO_PATH
    if not os.path.exists(path):
        Path(path).mkdir(parents=True)

    imgPath = f"{path}\\{imgname}.jpg"
    resp = requests.get(url, stream=True)
    try:
        with open(imgPath, 'wb') as local_file:                  
            resp.raw.decode_content = True
            shutil.copyfileobj(resp.raw, local_file)
            log.createLog(f'Image downloaded successfuly URL: {url}') 
    except:
        log.createLog(f"ERROR Downloading log logo URL: {url}")
    del resp